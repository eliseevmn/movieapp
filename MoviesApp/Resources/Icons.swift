//
//  Icons.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation
import SwiftUI

extension Image {
    static let plusIcon = Image(systemName: "plus")
    static let placeholderIcon = Image("placeholder")
    static let exclamationmarkIcon = Image(systemName: "exclamationmark.circle")
    static let checkmarkCircleFillIcon = Image(systemName: "checkmark.circle.fill")
}
