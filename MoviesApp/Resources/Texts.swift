//
//  Texts.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation

enum Texts {

    enum MovieList {
        static let noMovies = "No movies found"
        static let moviesTitle = "Movies"
    }

    enum AddMovie {
        static let nameTitle = "Name"
        static let yearTitle = "Year"
        static let addMovieTitle = "Add New Movie"
        static let saveMovie = "Save"
    }

    enum URLImage {
        static let placeholderTitle = "placeholder"
    }

    enum MovieListView {
        static let movieYearTitle = "Movie Year"
        static let movieGenreTitle = "Movie Genre"
    }

    enum GenreSelection {
        static let selectTitle = "Select"
    }
}
