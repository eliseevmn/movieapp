//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import SwiftUI

struct AddMovieScreen: View {

    // MARK: - Properties
    @EnvironmentObject var addMovieVM: AddMovieViewModel
    @Environment(\.presentationMode) private var presentationMode

    // MARK: - Body
    var body: some View {
        Form {
            TextField(Texts.AddMovie.nameTitle,
                      text: $addMovieVM.name, onEditingChanged: { _ in }) {
                addMovieVM.fetchPostersByMovieName(name: addMovieVM.name.encoded())
            }

            TextField(Texts.AddMovie.yearTitle,
                      text: $addMovieVM.year)

            GenreSelectionView(onSelected: { (genreModel) in
                addMovieVM.genre = genreModel.name
            }, ingnoredGenres: ["All"])

            MoviePosterGridView(posters: addMovieVM.posters, selectedPoster: $addMovieVM.poster)
        } // Form
        .navigationTitle(Texts.AddMovie.addMovieTitle)
        .navigationBarItems(trailing: Button(Texts.AddMovie.saveMovie) {
            addMovieVM.addMovie {
                presentationMode.wrappedValue.dismiss()
            }
        })
        .embedInNavigationView()
    }
}
