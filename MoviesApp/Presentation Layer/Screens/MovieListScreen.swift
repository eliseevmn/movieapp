//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import SwiftUI
import Combine

struct MovieListScreen: View {

    // MARK: - Properties
    @EnvironmentObject var movieListVM: MovieListViewModel
    @State private var isPresented: Bool = false

    // MARK: - Body
    var body: some View {
        switch movieListVM.state {
            case .isLoading:
                ProgressView()
                    .onAppear(perform: {
                        movieListVM.getAllMovies()
                    })
            case .success(let movies):
                successContent(movies: movies)
            case .failure(let error):
                Text(error.localizedDescription)
        }
    } // Body

    // MARK: - Helpers functions
    private func successContent(movies: [MovieViewModel]) -> some View {
        VStack {
            GenreSelectionView(onSelected: movieListVM.genreSelected)

            Spacer()

            if !movies.isEmpty {
                MovieListView(movies: movies) { movieId in
                    movieListVM.deleteMovie(movieId: movieId)
                }
            } else {
                NoResultView(message: Texts.MovieList.noMovies)
            }

            Spacer()
        } // VStack
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding()
        .navigationBarItems(trailing: Button(action: {
            isPresented = true
        }, label: {
            Image.plusIcon
        }))
        .navigationTitle(Texts.MovieList.moviesTitle)
        .embedInNavigationView()
        .sheet(isPresented: $isPresented, onDismiss: {
            movieListVM.getAllMovies()
        }, content: {
            AddMovieScreen()
        })
    }
}
