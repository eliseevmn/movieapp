//
//  DIContainer.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation

final class DIContainer {

    // MARK: - Properties
    let session: URLSession
    let decoder: JSONDecoder
    let requestBuilder: RequestBuilder
    let apiClient: APIClient
    lazy var moviesService = MoviesService(moviesAPIClient: apiClient)

    // MARK: - Initialisers
    init() {
        requestBuilder = RequestBuilder()
        session = URLSession.shared
        decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        apiClient = APIClient(requestBuilder: requestBuilder,
                              session: session,
                              decoder: decoder)
    }
}
