//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import SwiftUI

struct URLImage: View {

    // MARK: - Properties
    let url: String
    let placeholder: String
    @ObservedObject var imageLoader = ImageLoader()

    // MARK: - Init
    init(url: String, placeholder: String = Texts.URLImage.placeholderTitle) {
        self.url = url
        self.placeholder = placeholder
        self.imageLoader.downloadImage(url: self.url)
    }

    // MARK: - Body
    var body: some View {
        if let data = self.imageLoader.downloadedData {
            return Image(uiImage: UIImage(data: data)!).resizable()
        } else {
            return Image.placeholderIcon.resizable()
        }
    } // Body
}
