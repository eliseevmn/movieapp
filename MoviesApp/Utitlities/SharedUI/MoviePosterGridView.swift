//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import SwiftUI

struct MoviePosterGridView: View {

    // MARK: - Properties
    let posters: [PosterViewModel]
    @Binding var selectedPoster: String

    // MARK: - Body
    var body: some View {
        LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())],
                  content: {
            ForEach(posters, id: \.id) { poster in
                VStack {
                    URLImage(url: poster.poster)
                        .frame(width: 100, height: 150)
                        .clipShape(RoundedRectangle(cornerRadius: 16.0, style: /*@START_MENU_TOKEN@*/.continuous/*@END_MENU_TOKEN@*/))
                        .shadow(color: Color.grayApp, radius: 6, x: 8, y: 8)
                        .shadow(color: Color.whiteApp, radius: 6, x: -8, y: -8)
                        .overlay(
                                ZStack {
                                    Rectangle()
                                        .fill(Color.shadowColorApp)
                                    Image
                                        .checkmarkCircleFillIcon
                                        .foregroundColor(Color.whiteApp)
                                } // ZStack
                                .frame(maxHeight: isSelectedPoster(poster: poster.poster) ? 44: 0)
                                .clipped()
                                .animation(.spring())
                            , alignment: .bottom)
                        .onTapGesture {
                            selectedPoster = poster.poster
                        }
                } // VStack
            } // ForEach
        }) // LazyVGrid
    } // Body

    // MARK: - Helpers functions
    func isSelectedPoster(poster: String) -> Bool {
        return selectedPoster == poster
    }
}
