//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import SwiftUI

struct NoResultView: View {

    // MARK: - Properties
    let message: String

    // MARK: - Body
    var body: some View {
        HStack {
            Image
                .exclamationmarkIcon
                .foregroundColor(Color.yellowApp)
            Text(message)
        } // HStack
        .padding()
    } // Body
}
