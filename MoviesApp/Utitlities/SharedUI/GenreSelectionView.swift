//
//  GenreSelectionView.swift
//  MoviesApp
//
//  Created by MAC on 24.02.2022.
//

import SwiftUI

struct GenreSelectionView: View {

    // MARK: - Properties
    let onSelected: (GenreViewModel) -> Void
    @State private var selectedGenre: GenreViewModel = GenreViewModel.defaultGenre
    @EnvironmentObject private var genreListVM: GenreListViewModel
    var ingnoredGenres: [String]?

    // MARK: - Properties
    var body: some View {
        Picker(Texts.GenreSelection.selectTitle, selection: $selectedGenre) {
            ForEach(prepareGenres(), id: \.id) { genre in
                Text(genre.name).tag(genre)
            } // ForEach
        } // Picker
        .pickerStyle(SegmentedPickerStyle())
        .onAppear {
            genreListVM.getAllGenres()
        }
        .onChange(of: selectedGenre, perform: { value in
            onSelected(value)
        })
    } // Body

    // MARK: - Helpers functions
    func prepareGenres() -> [GenreViewModel] {
        guard let ingnoredGenres = ingnoredGenres else {
            return genreListVM.genres
        }

        return genreListVM.genres.filter { genre in
            return !ingnoredGenres.contains(genre.name)
        }
    }
}
