//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import SwiftUI

struct MovieListView: View {

    // MARK - Properties
    let movies: [MovieViewModel]
    var onDeleteMovie: ((String) -> Void)?

    // MARK: - Body
    var body: some View {
        List {
            ForEach(movies, id: \.id) { movie in
                HStack {
                    URLImage(url: movie.poster)
                        .frame(width: 100, height: 150)
                        .clipShape(RoundedRectangle(cornerRadius: 16.0, style: /*@START_MENU_TOKEN@*/.continuous/*@END_MENU_TOKEN@*/))

                    VStack(alignment: .leading) {
                        Text(movie.title)
                            .padding(.top, 10)
                            .font(.headline)
                        Text("\(Texts.MovieListView.movieYearTitle): \(movie.year)")
                            .font(.caption)

                        HStack {
                            Text("\(Texts.MovieListView.movieGenreTitle): \(movie.genre)")
                                .font(.caption)
                                .foregroundColor(Color.blackApp)
                                .padding(8)
                            
                        } // HStack
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.grayApp, lineWidth: 0.5)
                        )
                        
                        Spacer()
                    } // VSTack
                } // HStack
            }
            .onDelete(perform: deleteMovie)
        } // List
        .listStyle(PlainListStyle())
    } // Body

    // MARK: - Actions
    private func deleteMovie(at indexSet: IndexSet) {
        indexSet.forEach { index in
            let movie = movies[index]
            if let onDeleteMovie = onDeleteMovie {
                onDeleteMovie(movie.id)
            }
        }
    }
}
