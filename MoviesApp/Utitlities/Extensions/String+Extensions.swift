//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import Foundation

extension String {
    
    func encoded() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? self 
    }
    
}
