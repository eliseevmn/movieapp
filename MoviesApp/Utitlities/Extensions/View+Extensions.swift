//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import Foundation
import SwiftUI

extension View {
    func embedInNavigationView() -> some View {
        NavigationView { self }
    }
}
