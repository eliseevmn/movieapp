//
//  Color+.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import Foundation
import SwiftUI

extension Color {

    static let blackApp = Color("black")
    static let grayApp = Color("gray")
    static let whiteApp = Color("white")
    static let yellowApp = Color("yellow")
    static let shadowColorApp = Color("shadowColor")
}
