//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import SwiftUI

@main
struct MoviesAppApp: App {

    // MARK: - Properties
    private let diContainer = DIContainer()

    // MARK: - Body
    var body: some Scene {
        WindowGroup {
            MovieListScreen()
                .environmentObject(MovieListViewModel())
                .environmentObject(GenreListViewModel())
                .environmentObject(AddMovieViewModel(moviesService: diContainer.moviesService))
        } // WindowGroup
    } // Body
}
