//
//  GenreListViewModel.swift
//  MoviesApp
//
//  Created by MAC on 24.02.2022.
//

import Foundation
import Apollo

final class GenreListViewModel: ObservableObject {

    // MARK: - Properties
    @Published var genres: [GenreViewModel] = []

    // MARK: - Apollo functions
    /// Функция для парсинга всех объектов GenreViewModel
    func getAllGenres() {
        ApolloService.shared.apollo.fetch(query: GetAllGenresQuery()) { [weak self] result in
            switch result {
                case .success(let graphQLResult):
                    guard let data = graphQLResult.data,
                          let genres = data.genres
                        else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.genres = genres.compactMap { $0 }.map(GenreViewModel.init)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
    }
}
