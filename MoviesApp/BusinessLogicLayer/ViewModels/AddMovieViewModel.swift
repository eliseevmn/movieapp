//
//  AddMovieModel.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import Foundation
import Combine

class AddMovieViewModel: ObservableObject {

    // MARK: - Properties
    @Published var posters: [PosterViewModel] = []

    var name: String = ""
    var year: String = ""
    @Published var poster: String = ""
    var genre: String = ""

    private let moviesService: MoviesServiceProtocol?
    private var cancelable = Set<AnyCancellable>()

    // MARK: - Initializers
    init(moviesService: MoviesServiceProtocol?) {
        self.moviesService = moviesService
    }

    // MARK: - Apollo functions
    /// Функция для добавления фильма в общий лист
    /// - Parameter completion:
    func addMovie(completion: @escaping () -> Void) {
        let movie = MovieInput(title: name, year: year, genre: genre, poster: poster)

        ApolloService.shared.apollo.perform(mutation: CreateMovieMutation(movie: movie)) { result in
            switch result {
                case .success(_):
                    completion()
                case .failure(let error):
                    print(error)
            }
        }
    }

    // MARK: - Network functions
    /// Функция для получения объектов PosterViewModel
    /// - Parameter name: наименование фильма
    func fetchPostersByMovieName(name: String) {
        moviesService?
            .getAllMovies(name: name, pageNumber: 1)
            .receive(on: RunLoop.main)
            .map { $0.movies }
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetching movie finished")
                    case .failure(let error):
                        print(error.localizedDescription)
                }
            }, receiveValue: { movies in
                self.posters = movies.map(PosterViewModel.init)
            })
            .store(in: &self.cancelable)
    }
}
