//
//  MovieListViewModel.swift
//  MoviesApp
//
//  Created by MAC on 24.02.2022.
//

import Foundation
import Apollo

final class MovieListViewModel: ObservableObject {

    // MARK: - State
    enum State {
        case isLoading
        case success([MovieViewModel])
        case failure(Error)
    }

    // MARK: - Properties
    @Published private(set) var state: State

    // MARK: - Initializer
    init() {
        self.state = .isLoading
    }

    // MARK: - Apollo functions
    /// Функция для удаления объекта MovieViewModel
    /// - Parameter movieId: Id объекта MovieViewModel
    func deleteMovie(movieId: String) {
        ApolloService.shared.apollo.perform(mutation: DeleteMovieMutation(movieId: movieId)) { [weak self] result in
            switch result {
                case .success(_):
                    self?.getAllMovies()
                case .failure(let error):
                    print(error)
            }
        }
    }

    /// Функция для парсинга всех объектов MovieViewModel
    /// - Parameter genre: Наименование жанра
    func getAllMovies(genre: String? = nil) {
        ApolloService.shared.apollo.fetch(query: GetAllMoviesQuery(genre: genre),
                                          cachePolicy: .fetchIgnoringCacheData) { [weak self] result in
            switch result {
                case .success(let graphQLResult):
                    guard let data = graphQLResult.data,
                          let movies = data.movies
                          else {
                        return
                    }
                    DispatchQueue.main.async {
                        let movies = movies.compactMap { $0 }.map(MovieViewModel.init)
                        self?.state = .success(movies)
                    }
                case .failure(let error):
                    self?.state = .failure(error)
            }
        }
    }

    // MARK: - Helpers functions
    /// Функция выгрузка объектов MovieViewModel в зависимости от жанра
    /// - Parameter vm: Объект GenreViewModel
    func genreSelected(genreModel: GenreViewModel) {
        switch genreModel.name {
            case "All":
                getAllMovies()
            default:
                getAllMovies(genre: genreModel.name)
        }
    }
}
