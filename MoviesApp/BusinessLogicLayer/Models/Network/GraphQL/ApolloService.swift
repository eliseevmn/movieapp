//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import Foundation
import Apollo

class ApolloService {

    static let shared: ApolloService = ApolloService()

    private init() { }

    lazy var apollo = ApolloClient(url: URL(string: "http://localhost:4000")!)
}
