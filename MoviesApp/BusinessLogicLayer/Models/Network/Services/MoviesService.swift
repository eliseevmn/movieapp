//
//  MoviesService.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation
import Combine

protocol MoviesServiceProtocol {
    func getAllMovies(name: String, pageNumber: Int) -> AnyPublisher<MovieResponse, Error>
}

final class MoviesService: MoviesServiceProtocol {

    // MARK: - Properties
    private let moviesAPIClient: MoviesAPIClient

    // MARK: - Init
    init(moviesAPIClient: MoviesAPIClient) {
        self.moviesAPIClient = moviesAPIClient
    }

    // MARK: - Public Methods
    /// Функция для получения объекта MovieResponse содержащего объему MovieViewModel
    /// - Parameters:
    ///   - name: Наименование фильма
    ///   - pageNumber: номер страницы
    /// - Returns: Publisher c объектами
    func getAllMovies(name: String, pageNumber: Int) -> AnyPublisher<MovieResponse, Error> {
        return moviesAPIClient
            .getAllMovies(name: name, pageNumber: pageNumber)
            .eraseToAnyPublisher()
    }
}
