//
//  Movie.swift
//  MoviesApp
//
//  Created by MAC on 25.02.2022.
//

import Foundation

class ImageLoader: ObservableObject {

    // MARK: - Properties
    @Published var downloadedData: Data?

    // MARK: - Download Image functions
    /// Функция для загрузки изображения
    /// - Parameter url: URL изображеня
    func downloadImage(url: String) {
        guard let imageURL = URL(string: url) else {
            return
        }
        
        URLSession.shared.dataTask(with: imageURL) { data, _, error in
            guard let data = data, error == nil else {
                return
            }
            DispatchQueue.main.async {
                self.downloadedData = data 
            }
        }.resume()
    }
}
