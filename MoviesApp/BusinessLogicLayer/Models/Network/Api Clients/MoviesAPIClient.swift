//
//  MoviesAPIClient.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation
import Combine

protocol MoviesAPIClient {
    func getAllMovies(name: String, pageNumber: Int) -> AnyPublisher<MovieResponse, Error>
}

extension APIClient: MoviesAPIClient {

    /// Функция для получения объекта MovieResponse содержащего объему MovieViewModel
    /// - Parameters:
    ///   - name: Наименование фильма
    ///   - pageNumber: номер страницы
    /// - Returns: Publisher c объектами
    func getAllMovies(name: String, pageNumber: Int) -> AnyPublisher<MovieResponse, Error> {
        let request = requestBuilder
            .set(path: .getMovies)
            .set(method: .GET)
            .set(parameters: [
                "s": name,
                "page": "\(pageNumber)",
                "apikey": Constants.Keys.apiKey
            ])
            .build()
        return performRequest(request)
    }
}
