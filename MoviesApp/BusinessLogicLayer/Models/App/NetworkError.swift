//
//  NetworkError.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case noData
    case decodingError
}
