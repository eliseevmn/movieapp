//
//  GenreViewModel.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation

struct GenreViewModel: Identifiable, Hashable {

    let id = UUID()
    let genre: GetAllGenresQuery.Data.Genre

    var name: String {
        genre.name
    }

    static var defaultGenre: GenreViewModel {
        return GenreViewModel(genre: GetAllGenresQuery.Data.Genre(name: "All"))
    }

    static func == (lhs: GenreViewModel, rhs: GenreViewModel) -> Bool {
        return lhs.name == rhs.name
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}
