//
//  MovieViewModel.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation
import Apollo

struct MovieViewModel {

    let movie: GetAllMoviesQuery.Data.Movie

    var id: GraphQLID {
        movie.id
    }

    var title: String {
        movie.title
    }

    var year: String {
        movie.year
    }

    var genre: String {
        movie.genre
    }

    var poster: String {
        movie.poster
    }
}
