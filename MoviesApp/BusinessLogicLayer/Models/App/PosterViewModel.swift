//
//  PosterViewModel.swift
//  MoviesApp
//
//  Created by MAC on 01.03.2022.
//

import Foundation

struct PosterViewModel {

    let movie: Movie

    let id = UUID()

    var title: String {
        movie.title
    }

    var poster: String {
        movie.poster
    }
}
